# Lab set-up

## Requirements

* Docker CE
* `kubectl` binary
* `kind` binary

## Instructions

### CentOS Stream

> Tested on a fresh VM running [CentOS Stream 8](https://centos.org/centos-stream/).
> Skip to step 2 if you already have a working Docker set-up

```bash
# 1) Set up Docker CE
$ sudo yum install yum-utils.noarch
$ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
$ sudo yum install docker-ce docker-ce-cli containerd.io
$ sudo usermod -aG docker ${USER}
$ newgrp docker
$ sudo systemctl enable --now docker.service

# 2) Install kubectl
$ cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
$ sudo yum install -y kubectl

# 3) Download kind binary
$ mkdir -p ~/.local/bin/
$ curl -Lo ~/.local/bin/kind https://kind.sigs.k8s.io/dl/latest/kind-linux-amd64
$ chmod +x ~/.local/bin/kind

# 4) Set up shell auto completion
$ echo '. <(kubectl completion bash)'   >> ~/.bashrc
$ echo '. <(kind completion bash)' >> ~/.bashrc
$ . ~/.bashrc

# 5) Install extra packages
$ sudo yum install -y git make vim bash-completion

# 6) Logout
$ logout
```

### Ubuntu 20.04

> Tested on a fresh VM running [Ubuntu Server 20.04.2](https://ubuntu.com/download/server).
> Skip to step 2 if you already have a working Docker set-up

```bash
# 1) Set up Docker CE
$ sudo addgroup --system docker
$ sudo usermod -aG docker ${USER}
$ newgrp docker
$ sudo snap install docker

# 2) Install kubectl
$ mkdir -p ~/.local/bin/
$ curl -Lo ~/.local/bin/kubectl "https://dl.k8s.io/release/v1.22.0/bin/linux/amd64/kubectl"

# 3) Download kind binary
$ curl -Lo ~/.local/bin/kind https://kind.sigs.k8s.io/dl/latest/kind-linux-amd64
$ chmod +x ~/.local/bin/kind

# 4) Set up shell auto completion
$ echo '. <(kubectl completion bash)'   >> ~/.bashrc
$ echo '. <(kind completion bash)' >> ~/.bashrc
$ . ~/.bashrc

# 5) Install extra packages
apt install git make vim bash-completion

# 6) Logout
$ logout
```

