# Containers and Kubernetes workshop

In this workshop you will be building a container image and deploying that image to you local Kubernetes cluster.

## Containers

Objectives:
- Build a Apache container with a static website 
- Test is locally
- Push it to the local registry

### Apache
- Create the Apache Containerfile

- Use your preferred editor and create a new Containerfile:
```shell
mkdir -p /home/$(whoami)/httpd 
cd /home/$(whoami)/httpd

vim /home/$(whoami)/httpd/Containerfile
```

- Use UBI 8.3 as a base image by adding the following `FROM` instruction at the top of the new Containerfile:
```shell
FROM registry.access.redhat.com/ubi8/ubi:8.3
```

- Add a `RUN` instruction with a `yum install` command to install Apache on the new container:
```shell
RUN yum install -y httpd && \
    yum clean all
```

- Add a `RUN` instruction to replace contents of the default HTTPD home page:
```shell
RUN echo "Hello from Containerfile" > /var/www/html/index.html
```

- Use the `EXPOSE` instruction below the RUN instruction to document the port that the container listens to at runtime. 
In this instance, set the port to 80, because it is the default for an Apache server:
```shell
EXPOSE 80
```

---
**Note**

The EXPOSE instruction does not actually make the specified port available to the host; 
rather, the instruction serves as metadata about which ports the container is listening on.

---

- At the end of the file, use the following `CMD` instruction to set `httpd` as the default entry point:
```shell
CMD ["httpd", "-D", "FOREGROUND"]
```

- Verify that your Containerfile matches the following before saving and proceeding with the next steps:
```shell
FROM registry.access.redhat.com/ubi8/ubi:8.3

RUN yum install -y httpd && \
    yum clean all
    
RUN echo "Hello from Containerfile" > /var/www/html/index.html

EXPOSE 80

CMD ["httpd", "-D", "FOREGROUND"]
```

- Build and verify the Apache container image.

- Use the following commands to create a basic Apache container image using the newly created Containerfile:
```shell
docker build -t workshop/httpd . -f Containerfile
```

The container image listed in the `FROM` instruction is only downloaded if not already present in local storage.

- After the build process has finished, run docker images to see the new image in the image repository:
```shell
docker images

REPOSITORY                            TAG       IMAGE ID       CREATED         SIZE
workshop/httpd                        latest    8cbc3867e96c   7 seconds ago   226MB
```

- Run the Apache container

Use the following command to run a container using the Apache image:

```shell
docker run --name apache -d \
    -p 10080:80 \
    workshop/httpd
9c946ab8e30d7a57b2a1fe7db42ac6527593aa8d9b6558529ecf1fbff39f00df
```

Run the `docker ps` command to see the running container:
```shell
docker ps

CONTAINER ID   IMAGE                  COMMAND                  CREATED          STATUS          PORTS                                                                 NAMES
9c946ab8e30d   workshop/httpd         "httpd -D FOREGROUND"    3 seconds ago    Up 2 seconds    0.0.0.0:10080->80/tcp, :::10080->80/tcp                               apache
```

Use the curl command to verify that the server is running:
```shell
curl 127.0.0.1:10080
Hello from Containerfile
```

- Stop the apache container
```shell
docker stop apache
```

- Push the image to the internal registry
```shell
docker tag workshop/httpd:latest localhost:5000/httpd:1.0
```

```shell
docker push localhost:5000/httpd:1.0

The push refers to repository [localhost:5000/httpd]
90ebeb6f57b7: Pushed 
5d6c0021bc92: Pushed 
1e8cd6732429: Pushed 
476579af086a: Pushed 
1.0: digest: sha256:c0214f3732655625f331f9fc3cc6f608ab2d6cc05c9c2127857636b4248e4709 size: 1155
```

- Check if the image is in the registry
```shell
curl localhost:5000/v2/_catalog
{"repositories":["httpd"]}

curl localhost:5000/v2/httpd/tags/list
{"name":"httpd","tags":["1.0"]}
```

## Kubernetes

Objectives:
- Create a default Apache deployment
- Create a deployment of our own built image
- Create a service
- Create a ingress
- Test that the application is reachable

### Create you first deployment

- Create a deployment template 
```shell
kubectl create deployment first --image=nginx:latest --dry-run=client -o yaml > first.yaml
```

- Review the `first.yaml` file

- Create a namespace for our first deployment
```shell
kubectl create namespace first
namespace/first created
```

- Switch from the default namespace context to the first namespace
```shell
# permanently save the namespace for all subsequent kubectl commands in that context.
kubectl config set-context --current --namespace=first
Context "kind-kind" modified.
```
> Every command you run will now use the first namespace as the context.
> For example `kubectl get all` will show nothing.
> Running the same command with the kube-system namespace `kubectl -n kube-system get all` will show everything in that namespace

- Apply the first deployment 
```shell
kubectl apply -f first.yaml
```

- Check what kubernetes has deployed
```shell
kubectl get all

NAME                         READY   STATUS    RESTARTS   AGE
pod/first-744bbcb77b-fb999   1/1     Running   0          3m1s

NAME                    READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/first   1/1     1            1           3m1s

NAME                               DESIRED   CURRENT   READY   AGE
replicaset.apps/first-744bbcb77b   1         1         1       3m1s
```

### Create a deployment for our Apache application

- Create a deployment for our own built Apache image
```shell
kubectl create deployment second --image=localhost:5000/httpd:1.0 --dry-run=client -o yaml > second.yaml
```
- Review the `second.yaml` file

- Create a new namespace for our deployment
```shell
kubectl create namespace second
```

- Switch to the second namespace
```shell
kubectl config set-context --current --namespace=second
```

- Apply the second deployment 
```shell
kubectl apply -f second.yaml
```

- Check what kubernetes has deployed
```shell
kubectl get all
NAME                          READY   STATUS    RESTARTS   AGE
pod/second-597ddcd99d-56x6k   1/1     Running   0          51s

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/second   1/1     1            1           51s

NAME                                DESIRED   CURRENT   READY   AGE
replicaset.apps/second-597ddcd99d   1         1         1       51s
```

- Create a [service](https://kubernetes.io/docs/concepts/services-networking/service/) 
```shell
kubectl expose deployment second --port=80
```

```shell
kubectl get svc
NAME     TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
second   ClusterIP   10.96.15.99   <none>        80/TCP    11s
```

- 
```shell
kubectl create ingress ingress-second --rule="second.dev.nl/=second:80"
ingress.networking.k8s.io/ingress-second created

kubectl get ing
NAME             CLASS    HOSTS           ADDRESS   PORTS   AGE
ingress-second   <none>   second.dev.nl             80      9s
[root@centos ~]# 

```

- Edit the hosts file for access to the cluster
```shell
vi /etc/hosts
# or
sudo echo "127.0.0.1 second.dev.nl" >> /etc/hosts
```

- Test the ingress
```shell
curl second.dev.nl
Hello from Containerfile
```


## Rebuild
- change your Containerfile text to say "Hello World!"
```shell
FROM registry.access.redhat.com/ubi8/ubi:8.3

RUN yum install -y httpd && \
    yum clean all
    
RUN echo "Hello World!" > /var/www/html/index.html

EXPOSE 80

CMD ["httpd", "-D", "FOREGROUND"]
```

- Rebuild the container. Notice the `2.0` version.
```shell
docker build -t workshop/httpd:2.0 . -f Containerfile
```

```shell
docker images

REPOSITORY                            TAG       IMAGE ID       CREATED          SIZE
workshop/httpd                        2.0       634fc6485df0   4 seconds ago    226MB
```

- Tag the image for pushing to the repository
```shell
docker tag workshop/httpd:2.0 localhost:5000/httpd:2.0
```

- Push the image to the local repository
```shell
docker push localhost:5000/httpd:2.0
```

- Create a third deployment in the second namespace using the 2.0 version
```shell
kubectl create deployment third --image=localhost:5000/httpd:2.0 
```

```shell
kubectl get all
NAME                          READY   STATUS    RESTARTS   AGE
pod/second-597ddcd99d-56x6k   1/1     Running   0          80m
pod/third-5865cdf998-vhtxr    1/1     Running   0          36s

NAME             TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
service/second   ClusterIP   10.96.15.99   <none>        80/TCP    71m

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/second   1/1     1            1           80m
deployment.apps/third    1/1     1            1           37s

NAME                                DESIRED   CURRENT   READY   AGE
replicaset.apps/second-597ddcd99d   1         1         1       80m
replicaset.apps/third-5865cdf998    1         1         1       36s
```

- Lets edit the service to also match the pods for the third deployment.
Replace the line for the selector `app: second` with `webserver: httpd`.
```shell
kubectl edit svc second
```

```yaml
spec:
  clusterIP: 10.96.15.99
  clusterIPs:
  - 10.96.15.99
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    webserver: httpd
  sessionAffinity: None
  type: ClusterIP
```

- Add a label to the current running pods
```shell
kubectl label pods second-67655cd68f-7czk6 third-6f4b5857f7-rbcx9 webserver=httpd
```
> Your pod names will be different

- Test the ingress again and you should see output from both pods
```shell
while true; do curl second.dev.nl; sleep 2; done
Hello World!
Hello from Containerfile
Hello World!
Hello from Containerfile
Hello World!
...
```

- As last lets say our third application is getting more requests, so we need more resources

Lets add a extra pod to the deployment.

```shell
kubectl scale --replicas=2 deployment third
deployment.apps/third scaled

kubectl get all
NAME                          READY   STATUS    RESTARTS   AGE
pod/second-67655cd68f-7czk6   1/1     Running   0          9m33s
pod/third-6f4b5857f7-787h7    1/1     Running   0          2s
pod/third-6f4b5857f7-rbcx9    1/1     Running   0          9m14s

NAME             TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
service/second   ClusterIP   10.96.15.99   <none>        80/TCP    89m

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/second   1/1     1            1           98m
deployment.apps/third    2/2     2            2           18m

NAME                                DESIRED   CURRENT   READY   AGE
replicaset.apps/second-597ddcd99d   0         0         0       98m
replicaset.apps/second-67655cd68f   1         1         1       9m33s
replicaset.apps/third-5865cdf998    0         0         0       18m
replicaset.apps/third-6f4b5857f7    2         2         2       9m14s
```

> Notice two running third pods


## The end